package engine.util;

import org.lwjgl.BufferUtils;

import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

import static org.lwjgl.BufferUtils.createByteBuffer;

public class BufferUtil {

	private static ByteBuffer resizeBuffer(ByteBuffer buffer, int newCapacity) {
		ByteBuffer newBuffer = BufferUtils.createByteBuffer(newCapacity);
		buffer.flip();
		newBuffer.put(buffer);
		return newBuffer;
	}

	public static ByteBuffer ioResourceToByteBuffer(String resource, int bufferSize){
		ByteBuffer buffer = null;
		try{
		try (
			InputStream source = Class.class.getResourceAsStream("/" + resource);
			ReadableByteChannel rbc = Channels.newChannel(source)){
				buffer = createByteBuffer(bufferSize);
				while (true) {
					int bytes = rbc.read(buffer);
					if (bytes == -1)
						break;
					if (buffer.remaining() == 0)
						buffer = resizeBuffer(buffer, buffer.capacity() * 2);
				}
			}
			buffer.flip();
		}catch (Exception e){
			Log.logError("Failed to load Resource to ByteBuffer: " + resource);
		}
		return buffer;
	}
}