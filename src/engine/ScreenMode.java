package engine;

public enum ScreenMode {
	BORDERLESS, FULLSCREEN, WINDOW
}